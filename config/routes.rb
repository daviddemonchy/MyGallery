Rails.application.routes.draw do
  root 'galleries#index'

  resources :galleries, only: [:index, :show]
  # une autre façon d'écrire les 2 routes générées par la ligne précédente
  # get 'galleries', 'galleries#index', as: 'galleries'
  # get 'galleries/:id', 'galleries#show', as: 'gallery'

  namespace :admin do
    resources :galleries
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
