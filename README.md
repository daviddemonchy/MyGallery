# TP POPSCHOOL LENS RAILS

Ce TP ne parlera pas des tests.

## 1/ On crée l'application

 - en  précisant la base de données avec l'option  **-d mysql **

```shell
$ rails new MyGallery -d mysql
$ cd MyGallery
```

## 2/ on crée le dépôt git et on le configure si besoin
```shell
$ git init .
$ git config --local user.email "dd@pop.eu.com"
```

## 3/ **On oublie pas** de protéger son fichier de configuration pour la base de données
- L'idée et de se le garder pour le bon fonctionnement de l'application et de laisser un modèle dans le dépôt.


```shell
$ echo 'database.yml' > .gitignore
$ cp config/database.yml config/database.yml.sample
```

## 4 On crée le dépôt

Je vous laisse créer un dépôt **public** sur framagit, github ou autre.

## 5  add / commit / push

_Sur le git remote vous utilisez vote url ssh de dépôt._

```
$ git add .
$ git commit -m 'initial commit'
$ git remote add origin git@framagit.org:daviddemonchy/MyGallery.git
$ git push -u origin master
```
## 6 Votre dépôt est créé
 commit [3f9731cb](https://framagit.org/daviddemonchy/MyGallery/commit/3f9731cb533f8316d6a62625b263f02274c27473)


## 7 La gallery  

### 7.1 On va s'occuper de créer l'espace admin tout de suite

- installation d'une gem assez pratique pour ce que l'on a à faire
[rails-admin-scaffold](https://github.com/dhampik/rails-admin-scaffold)

```shell
$ echo "gem 'rails-admin-scaffold'" >> Gemfile
$ bundle update
```

### 7.2 Création de la gallery pour la partie admin _via la gem_

```shell
$ rails g admin:scaffold_controller Gallery name:string published:boolean
$ rails g model Gallery name:string published:boolean
```

commit [6d28e7b7](https://framagit.org/daviddemonchy/MyGallery/commit/6d28e7b76c6bad27db2c8ca347bc6dda19374761)


## 8 Creation de la bdd et migration


### 8.1 configuration du fichier database.yml

  ```yaml
  default: &default
  adapter: mysql2
  encoding: utf8
  pool: 5
 username: YOUR LOGIN
  password: YOUR PASSWORD
  socket: /tmp/mysql.sock
```

### 8.2 Création de la bdd
```shell
$ rails db:create
```

### 8.3 migration
```shell
$ rails db:migrate
```

commit [fbafb5b4](https://framagit.org/daviddemonchy/MyGallery/commit/fbafb5b42f547ee73420e179a2228edb6a47d7ce)

## 9 Création du controller pour la partie frontend


### 9.1  Création
```shell
$ rails g scaffold_controller Gallery name:string published:boolean
```

### 9.2 Épuration

Nous n'avons pas besoin de modifier, ajouter, effacer de galleries via le frontoffice, c'est fait via le backoffice

On peut donc effacer les parties non nécessaires

Donc tout ce qui se rapporte à autre chose que _index_ et _show_ dans la partie du controller galleries_controller
mais aussi les fichiers pour les vues (app/views/galleries).

commit [2f79678d](https://framagit.org/daviddemonchy/MyGallery/commit/2f79678dd9986359cf8f96d4d33237c964be9e99)


## 10  Définition d'une route par default


Édition du fichier
```shell
config/routes.rb
```

Vous avez
```ruby
Rails.application.routes.draw do
  namespace :admin do
    resources :galleries
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
```

Et on ajoute une route _root_

le mot clé est **root**  et on y ajoute le controller, action voulu.
Dans notre cas, il s'agit du controller galleries  et de l'action index.

```ruby
Rails.application.routes.draw do
  root 'galleries#index'

  namespace :admin do
    resources :galleries
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
```

commit : [1db840fd](https://framagit.org/daviddemonchy/MyGallery/tree/1db840fdbdb1976fac5035b2e56e40d04152cc5c)

## 11 premier lancement de site.


### 11.1

L'application est lançable, elle ne ressemble pas à grand chose pour le moment, on pourrait passer par un peu de design boottrap par exemple, mais cette partie sera abordé plus tard.

Nous avons donc pour le moment créé une partie administrable pour nos galeries et une partie front qui pour le moment ressemble beaucoup à la partie admin.

```shell
$ rails server
```
ou son petit frère raccourci
```shell
$ rails s
```

Cette commande va lancer notre application en mode développement.
```shell
Puma starting in single mode...
* Version 3.7.1 (ruby 2.4.0-p0), codename: Snowy Sagebrush
* Min threads: 5, max threads: 5
* Environment: development
* Listening on tcp://localhost:3000
```

Vous pouvez vous rendre sur l'url [http://localhost:3000](http://localhost:3000)

commit [8711ef5e](https://framagit.org/daviddemonchy/MyGallery/commit/8711ef5e16db22b9bb50248561a35cd43cfbc962)

### 11.2  C'est le drame  :)

```text

NameError in Galleries#index

Showing /Users/fusco/git/framagit/popschool/tp/MyGallery/app/views/galleries/index.html.erb where line #29 raised:

undefined local variable or method `new_gallery_path' for #<#<Class:0x007fd3c2321368>:0x007fd3c23147d0>
Did you mean?  new_admin_gallery_path
```

Votre application vient de créer une erreur car elle ne connait pas le _path_ **new_gallery_path**

En effet, c'est une vue qui a été généré via la commande **scaffold**

Pour obtenir les _path_ disponible dans l'application vous pouvez utilisez la commande

```shell
$ rails routes
Prefix              Verb   URI Pattern                         Controller#Action
           root     GET    /                                   galleries#index
admin_galleries     GET    /admin/galleries(.:format)          admin/galleries#index
                    POST   /admin/galleries(.:format)          admin/galleries#create
new_admin_gallery   GET    /admin/galleries/new(.:format)      admin/galleries#new
edit_admin_gallery  GET    /admin/galleries/:id/edit(.:format) admin/galleries#edit
admin_gallery       GET    /admin/galleries/:id(.:format)      admin/galleries#show
                    PATCH  /admin/galleries/:id(.:format)      admin/galleries#update
                    PUT    /admin/galleries/:id(.:format)      admin/galleries#update
                    DELETE /admin/galleries/:id(.:format)      admin/galleries#destroy
```


Si on regarde la colonne _préfix_ on peut s'apercevoir que le le _path_ **new_gallery_path** n'existe pas.

Effectivement dans notre fichier **routes.rb**, nous n'avons rien spécifié à ce sujet.

En effet pour la partie frontend nous n'avons pas besoin d'ajouter une galerie cette action est réservée à l'admin.

on peut donc tout simplement supprimer cette partie, on va en profiter pour en faire de même pour le reste des liens inutilisés.

commit : [30286060](https://framagit.org/daviddemonchy/MyGallery/commit/302860607647932a7899608bdfb09474b81e7b44)

Cette fois ci l'url  [http://localhost:3000](http://localhost:3000) vous affiche quelque chose.

### 11.3   Ajout de la ressource galleries

Bien que l'url [http://localhost:3000](http://localhost:3000)  fonctionne, [http://localhost:3000/galleries](http://localhost:3000/galleries) ne fonctionnent pas.

Il faut donc ajouter les routes nécessaire dans le fichier _routes.rb_


```ruby
Rails.application.routes.draw do
  root 'galleries#index'

  resources :galleries, only: [:index, :show]
  # une autre façon d'écrire les 2 routes générées par la ligne précédente
  # get 'galleries', 'galleries#index', as: 'galleries'
  # get 'galleries/:id', 'galleries#show', as: 'gallery'

  namespace :admin do
    resources :galleries
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

```


commit []()
